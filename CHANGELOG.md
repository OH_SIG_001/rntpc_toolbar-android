# Changelog
## v0.2.2-rc.1
- feat: add HarmonyOS support for toolbar-android ([4c7192a1](https://gitee.com/openharmony-sig/rntpc_toolbar-android/commit/4c7192a18acf6eac333694257118210a0b045775))
- chore: remove implementation of other platforms ([ba9ae6a1](https://gitee.com/openharmony-sig/rntpc_toolbar-android/commit/ba9ae6a17c0bdff310a171727ea8fee3cf8cd415))
- chore: add README.OpenSource and COMMITTERS.md ([5dd5f19f](https://gitee.com/openharmony-sig/rntpc_toolbar-android/commit/5dd5f19fbb0a43f7e451defbe8936062f4d95643))
- pre-release: @react-native-ohos/toolbar-android@0.2.2-rc.1 ([23357c34](https://gitee.com/openharmony-sig/rntpc_toolbar-android/commit/23357c34d1f37ba74f0545787598de72b9728ce1))