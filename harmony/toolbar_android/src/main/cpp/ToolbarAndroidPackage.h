/*
 * Copyright (c) 2024 Huawei Device Co., Ltd. All rights reserved
 * Use of this source code is governed by a MIT license that can be
 * found in the LICENSE file.
 */

#pragma once

#include "generated/RNOH/generated/BaseToolbarAndroidPackage.h"

namespace rnoh {
class ToolbarAndroidPackage : public BaseToolbarAndroidPackage {
    using Super = BaseToolbarAndroidPackage;
    using Super::Super;
};
} // namespace rnoh